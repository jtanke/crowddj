** Project name: Crowd DJ


** Project repository: https://bitbucket.org/jtanke/crowddj


** Team members: Josh Tanke, Christopher Johnson


** Description (1-3 paragraphs):


Crowd DJ allows users to vote on the music selection at gatherings (parties, restaurants, etc).  User�s must have access to the venue�s playlist (login information) to be able to vote on the music selection.  The owner of the playlist can set preferences that filter what genres can be added to the queue.  Music order can also be set to random shuffle or priority queue (based on the number of votes for a song).


** Implementation approach:


The client/server component will be written in C++, and we will implement the spotify api using node.js. 


** Risk management plan (max 1 paragraph):


We will set priority objectives for the upcoming week (i.e. what action items need to get implemented), specific for each team member. They are responsible for attempting the item, and getting help if needed. If more than 2 action items are not met adequately, we will hold a team meeting, and then schedule an intervention with Professor Norris.