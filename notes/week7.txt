This week has been mainly developing the plan we outlined in the previous week.  Most discussion has been about optimization our current code rather than implementing anything new.

Technologies discussed:
unistd.h for HTTP protocol (GET and POST methods for the server)

Progress:
The SpotifyAPI class has been completed.  The server can now feed HTML to the client and authorize the user's Spotify credentials.  The next step will be connecting the 'host' users to their guests.